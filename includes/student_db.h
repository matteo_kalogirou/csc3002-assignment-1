/*
    HEADER FILE
    Holds all the function declarations
*/

#ifndef _student_db_h
#define _student_db_h

#include<string>
#include<iostream>
#include<vector>

//Struct
//Create a StudentRecord Struct
struct StudentRecord{
    std::string Name;
    std::string Surname;
    std::string StudentNumber;
    std::string ClassRecord;
};

//Fucntion Declarations
void clear(void);
void showMenuOptions(void);
void AddStudent(std::vector<StudentRecord> &db);
void ReadDB(std::string filename,
            std::vector<StudentRecord> &db);
void SaveDB(const std::vector<StudentRecord> &db);
void DisplayStudent(const std::vector<StudentRecord> &db);
void GradeStudent(const std::vector<StudentRecord> &db);
int indexOfStudent( const std::vector<StudentRecord> &db,
                    std::string snum);
bool searchDB(const std::vector<StudentRecord> &db,
              std::string id);
void createStudent(std::vector<StudentRecord> &db,
                            std::string name, 
                            std::string sname, 
                            std::string studnum, 
                            std::string record);



#endif