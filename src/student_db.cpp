/*
    SOURCE FILE
    Holds all of the function definitions
*/

#include "student_db.h"
#include <fstream>
#include <iomanip>
#include <sstream>

using namespace std;


//==== GENERAL FUNCTIONS
void clear(void)
{system("clear");}

void showMenuOptions(void)
{
    cout<<"(1) Add a student\n";
    cout<<"(2) Read in student data from a file\n";
    cout<<"(3) Save the database to a file\n";
    cout<<"(4) Display a student's data\n";
    cout<<"(5) Grade a student\n";
    cout<<"(q) Quit\n";
}

/*
    Create a new studentRecord given a student's information
    New entry is pushed to the back of StudentRecords vector
*/
void createStudent(vector<StudentRecord> &db, string name, string sname, string studnum, string record) 
{
    db.push_back(StudentRecord());
    db[db.size()-1].Name = name;
    db[db.size()-1].Surname = sname;
    db[db.size()-1].StudentNumber = studnum;
    db[db.size()-1].ClassRecord = record;
}

/* 
    Search a vector of structs for an element matching the given id
    Search key = student number
    Returns 1 if found, 0 otherwise;

    (const vector passed by reference so that it cannot be modified)
*/
bool searchDB(const vector<StudentRecord> &db, string id)
{
    for(unsigned int i=0; i<db.size(); i++)
    {
        if(db[i].StudentNumber.compare(id) == 0){
            return true;
        }
    }
    return false;
}

//==== MENU OPTION FUNCTIONS

/*
    Add a student to the existing database.
    First checks if the student exists. If not, a new student is created
    and added to the DB.
    Inputs: student DB, studentRecord vector
*/
void AddStudent(vector<StudentRecord> &db){
    string  name, sname, snum, rec;
    cout<<"Adding a student to the database...\n";
    cout<<"Please enter the student's credentials and then press return." << endl;
    cout << "Name: "; cin >> name;
    cout << "Surname: "; cin >> sname;
    cout << "Student Number: "; cin >> snum;
    cout << "Class record in the form [mark1] [mark2] ... [markN]: ";    
    getline(cin, rec);
    getline(cin, rec);

    //Search for student
    if( searchDB(db, snum) ){
        cout << "Student " << name << " " << sname << " already exists in the database." << endl;
        cout << "Student not added." << endl<<endl;
    }else{
        createStudent(db, name, sname, snum, rec);
        cout << "Student " << db[db.size()-1].StudentNumber << " added successfully!" << endl << endl;
    }

    
}

/*
    Read in a file containing student records
    Uses an input file stream and results are stored in a StudentRecords vector
    Inputs: filename
    Output: a vector of studentRecords
*/
void ReadDB(string filename, vector<StudentRecord> &db){

    string name, sname, snum, rec;
    // vector<StudentRecord> newDB;
    
    cout << "Accessing "<<filename <<"..." << endl;

    ifstream inpfile (filename);
    string line;

    if(inpfile.is_open())
    {
        //Discard the first line
        getline(inpfile, line);

        while(!inpfile.eof())
        {    
            inpfile >> name >> sname >> snum;
            getline(inpfile, rec);
            //trim rec
            rec = rec.substr(rec.find_first_not_of(" "), rec.size());

            //Check for students already there
            if( !searchDB(db, snum) )
                createStudent(db, name, sname, snum, rec);            
        }

        inpfile.close();
    }else
        cout << "Unable to open file '"<<filename<<"'.\n\n";


}

/**
 * Saves the current database into a textfile.
 */
void SaveDB(const vector<StudentRecord> &db){

    string filename;

    cout<<"Enter the name of the file to save and press return, eg 'filename.txt'."<<endl;
    cin >> filename;

    ofstream ofs (filename, ofstream::out);
    
    //Make Headings
    ofs << "NAME"
        << setw(40) << "SURNAME"
        << setw(40) << "STUDENT NUMBER"
        << setw(40) << "CLASS RECORD \n";

    for(unsigned int i=0; i<db.size(); i++)
    {
        ofs << db[i].Name
            << setw(40) << db[i].Surname    
            << setw(40) << db[i].StudentNumber
            << setw(40) << db[i].ClassRecord << endl;
    }

    
    cout<<"Saving...\n";
    ofs.close();
    cout<<"Save successfull!"<<endl;
}

/**
 *  Displays the student with the given student number from the db.
 */
void DisplayStudent(const vector<StudentRecord> &db){

    string snum;
    cout << "Enter the student number: "; cin >> snum;
    cout << "Searching for student: " << snum << endl;
    
    int index = indexOfStudent(db, snum);
    if(index < 0){
        cout << "Student not found.";
    }
    else
    {
        cout << db[index].Name << " "
             << db[index].Surname << " "
             << db[index].StudentNumber << " "
             << db[index].ClassRecord << endl;
    }   
}

/**
 * Calculate the average of a student's grades
 */
void GradeStudent(const vector<StudentRecord> &db){
    string snum;
    float sum=0, temp, j=0;
    cout<<"Enter the student number: "; cin >> snum;
    int index = indexOfStudent(db, snum);

    if(index < 0)
    {
        cout << "Student not found in the database."<<endl;        
    }else
    {
        //Student exists, get average
        istringstream iss(db[index].ClassRecord);
        while(!iss.eof()){
            j++;
            iss >> temp;
            sum += temp;
        }

        cout << db[index].StudentNumber<< "'s Class Record: \n";
        cout << db[index].ClassRecord << endl;
        cout << "Average = " << sum/j << endl;
    }

}

int indexOfStudent(const vector<StudentRecord> &db, string snum)
{
    for (unsigned int i=0; i<db.size(); i++)
        if(!db[i].StudentNumber.compare(snum))
            return i;

    return -1;
}