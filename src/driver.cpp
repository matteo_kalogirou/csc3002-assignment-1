/*
 DRIVER PROGRAM:
 Executes the main program.
*/

#include"student_db.h"
#include<cstdlib>           //used to make system calls

using namespace std;

int main(void)
{

cout << "Welcome to the Student Database.\n" << endl;

//Variable Definition
char response;
string filename;
vector<StudentRecord> studentRecords;   //DB of student records

    //Infinite Loop
    for(;;)
    {
        cout << "Please make a selection and press return. Enter 'q' to quit." << endl;
        showMenuOptions();
        cin >> response;
        //Switch statement to categorise the response
        switch(response){
            case '1':   //Add a student
                        clear();
                        //Call AddStudent()
                        AddStudent(studentRecords);
                        break;

            case '2':   //Read DB
                        clear();
                        cout << "Enter the name of the file to open, eg 'filename.txt'\n";
                        cin >> filename;
                        ReadDB(filename, studentRecords);
                        cout << "Student database loaded." << endl;
                        break;

            case '3':   //Save DB
                        clear();
                        SaveDB(studentRecords);
                        //Call SaveDB()
                        break;

            case '4':   //Display given student data
                        clear();
                        DisplayStudent(studentRecords);
                        cout << endl;
                        break;

            case '5':   //Grade Student
                        clear();
                        GradeStudent(studentRecords);
                        //Call GradeStudent();
                        break;

            case 'q':   //Quit
                        clear();
                        cout<<"Are you sure you want to quit? (y/n)\n\n";
                        cin>>response;
                        if(response=='y')
                            return 0;
                        else if(response=='n')
                            break;
            default:    //Invalid entry
                        clear();
                        cout<<"Reponse not recognised. Please enter a valid response and then press return (1-5 or q to quit).\n\n";
        }

    }//end inf loop
    return 0;
}