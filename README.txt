Matteo Kalogirou
KLGMAT001


Use the makefile to compile the program.

student_db.h is the header file and hold all of the necessary function declarations. This is stored in /includes

student_db.cpp is the source file which describes all of the functionality of the program.
A StudentRecord struct is defined in the header which is used to describe a student (name, surname, student number, class record). Found in /src

The driver.cpp file executes the code from main(). Found in /src
In the driver, a vector of StudentRecord type is created and used to store all data.

Data can be read in from a text file in the following format:
    [Name] [Surname] [Student Number] [Class Record]
Entries are seperated by a space. The file's first line must have headings describing the fields.
Class record is a string of any number of values seperated by a space.

Data can be written to a file by specifying the file name, ie "data.txt".

Menu inputs are 1-5 and 'q'

Make clean = clean all object files.